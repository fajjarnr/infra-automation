terraform {
  backend "s3" {
    bucket = "devsecops-bucket-infra"
    key = "infra/state.tfstate"
    region = "us-east-2"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.3"
    }
  }
}
